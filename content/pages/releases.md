title: Releases
save_as: releases.html

<a id="install_semantic_versioning"></a>

Please note BuildStream follows the Semantic Versioning Convention
([SemVer](https://semver.org/)) and uses even minor point numbers to
denote releases intended for users while odd minor point numbers
represent development snapshops.

For example, for a given version number `X.Y.Z`

 - The `X.<even number>.*` versions are releases intended for users.
 - The `X.<odd number>.*` versions are development spanshots intended for testing.

## Releases

Latest version:
<object style="vertical-align: middle" data="https://buildstream.gitlab.io/buildstream/_static/release.svg" type="image/svg+xml">
(your browser does not support SVG, please find releases at [https://download.gnome.org/sources/BuildStream/](https://download.gnome.org/sources/BuildStream/).
</object>
[See the new features](feature.html).

| Version | URL | SHA256 |
|:-------:|:---:|:------:|
_download_table_stable:| [{version}]({anouncement}) | [{basename}]({uri}) | {sha256} |

## Development snapshots

Latest version:
<object style="vertical-align: middle" data="https://buildstream.gitlab.io/buildstream/_static/snapshot.svg" type="image/svg+xml">
(your browser does not support SVG, please find releases at [https://download.gnome.org/sources/BuildStream/](https://download.gnome.org/sources/BuildStream/).
</object>

| Version | URL | SHA256 |
|:-------:|:---:|:------:|
_download_table_unstable:| [{version}]({anouncement}) | [{basename}]({uri}) | {sha256} | [{news-basename}]({news}) |
