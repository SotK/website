title: feature page
save_as: feature.html

<!-- Feature page. Check the content structure to better understand the relation with other pages: https://gitlab.com/BuildStream/nosoftware/alignment/blob/master/content_design/content_structure_proposal_description.md -->

## BuildStream 1.2

[[_TOC_]]

### Release highlights

<!-- Text focused on those who can become users and current users, that is, we need to assume they have some technical knowledge. This text should mention and include a short description of the 2 or 3 features that makes a difference, that makes this release worth it. -->

Intro paragraph

Feature 1, Feature 2 and Feature 3 paragrapgh description

Why you should try it out.

### What's new

<!-- Table 1.0 vs 1.2 The idea is to reflect evolution and to inform about the updates in features, components and plugins or other elements   -->

What's new - Feature table: 

<!-- List key features for the general audience, then those for our target market and audience. Those who are only partially supported and will be fully supported in the next major version, too.   -->




| Feature      | v 1.0           | v 2.0       |
| :----------: | :------:     | :----------------------: |
|  CAS         |              | Implemented x and y. z missing |
|              |              |                |
|              |              |                |
| Cell 1       | Cell 2   | Cell 3             | 
| Cell 7       | Cell 8   | Cell 9             | 



What's new software components table:

<!-- List key software components for the general audience, then those for our target market and audience. There are people that has installed in their machines software versions that might collide or not be appropiate for running BuildStream. We need to let them know here.   -->

| Component      | v 1.0           | v 2.0       |
| :----------: | :------:     | :----------------------: |
|           |     v2.7         |  v2.8  |
|              |              |                |
|              |              |                |
| Cell 1       | Cell 2   | Cell 3             | 
| Cell 7       | Cell 8   | Cell 9             | 

What's new -  plugins and third party software table:

<!-- List key plugins and external software components that enable interesting or complementary features we partly or entirely rely on. for the general audience, then those for our target market and audience. Those who are only partially supported and will be fully supported in the next major version, too.   -->

| Component      | v 1.0           | v 2.0       |
| :----------: | :------:     | :----------------------: |
|           |     v2.7         |  v2.8  |
|              |              |                |
|              |              |                |
| Cell 1       | Cell 2   | Cell 3             | 
| Cell 7       | Cell 8   | Cell 9             | 


### Download and install

Paragraph pointing people to what they have to do in order to download and install BuildStream. Consider the difference between  release and snapshots as well as the different platforms we currently support.

<!-- This paragraph is to provide context and redirect readers to the right content, not to duplicate content from other pages or documentation. Remember the target audience is people wanting to install BuildStream for the first time.   -->

### Important Links

List and provide context to the main links required by a new user to consume BuildStream. Do not repeat the ones from the previous section. Recommend to go through the below pages in the proposed order.

<!-- Based on the critical path the order is Known issues, FAQ, BuildStream in detail   -->

* Known issues: short description about what is this page for and why should visit it.
* FAQ: short description about what is this page for and why should visit it. Insist on the section that has to be read (user section, the second. Link to that one, not to the title of the page.)
* BuildStream In Detail: short description about what is this page for and why should visit it.

Further links:
* News file (on gitlab): short description (for context).
* Readme file (on gitlab): short description (for context).
* Roadmap: short description (for context).
