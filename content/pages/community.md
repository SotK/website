title: Community
save_as: community.html

<!-- This is the so called project page  -->
[TOC]

## BuildStream Manifest

Coming soon.

## BuildStream Governance
<!-- This section describes the governance aspects of the project, including the licenses (link to the license page of the buildstream repo) and the project sponsors, as well as the relation with GNOME. -->
BuildStream is a Free Software project hosted by the [GNOME Foundation] so it inherits its Governance model and [Code of Conduct].

## License

BuildStream code and technical documentation is licensed under the [LGPL2.1] license. The website contents are licensed under [CC-BY-SA 4.0].

## Participate in the BuildStream project

The BuildStream project welcome and promotes the participation and contributions of any person. The following links will redirect you to what you need to know in order to participate:

* The most obvious way to participate is to [use BuildStream].
* If you are interested in testing the new features developed by the BuildStream community, install/deploy the latest [Development Snapshot].
      * It would be great to hear back about your experience. Join us on the #buildstream channel on the [GNOME IRC server] or the [BuildStream Community] mailing list. Feel free to provide support to other users through these channels.
            * You might also be interested in checking the [mailing list archive].
      * One of the most valuable ways to participate is [reporting potential bugs]. You can find a detailed explanation about how to do so in the [FAQ](faq.html).
* Develop BuildStream
      * The [buildstream repository] on gitlab hosts the code base. Sign-up on [Gitlab.com] and request becoming a member of the [BuildStream Group].
      * Make sure you read and understand the BuildStream [development policies and guidelines].
      * We recommend you to start by evaluating and potentially fixing [simple bugs], then move into reviewing [Merge Requests] already submitted and once you gain enough confidence, start submitting patches.
* Every [BuildStream meeting] is open to contributors, users and those simply interested in the project. Check the [BuildStream calendar] to find out about the next meeting.
* Very cool but I am not a developer!
      * As mentioned above, BuildStream welcomes contributors of all skill sets. You can add value [reviewing and developing contents], helping out on the [website], doing translations, promoting the tool etc. Join the mailing list, open a ticket, show up in one of our meetings, ping us through IRC or reach out to a community member at any event... everybody is welcome.

[GNOME Foundation]: https://wiki.gnome.org/Foundation/Bylaws
[Code of Conduct]: https://wiki.gnome.org/action/show/Foundation/CodeOfConduct?action=show&redirect=CodeOfConduct
[LGPL2.1]: https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html
[CC-BY-SA 4.0]: https://creativecommons.org/licenses/by-sa/4.0/
[use BuildStream]: install.html
[Development Snapshot]: releases.html#development-snapshots
[reporting potential bugs]: faq.html#contribute-to-buildstream
[GNOME IRC server]: https://wiki.gnome.org/Community/GettingInTouch/IRC
[BuildStream Community]: https://mail.gnome.org/mailman/listinfo/buildstream-list
[mailing list archive]: https://mail.gnome.org/archives/buildstream-list/
[repository buildstream]: https://gitlab.com/BuildStream/buildstream
[Gitlab.com]: http://www.gitlab.com
[BuildStream Group]: https://gitlab.com/BuildStream
[development policies and guidelines]: https://buildstream.gitlab.io/buildstream/HACKING.html
[simple bugs]: https://gitlab.com/BuildStream/buildstream/boards/580464?=&label_name[]=Bug
[Merge Requests]: https://gitlab.com/BuildStream/buildstream/merge_requests
[BuildStream meeting]: https://wiki.gnome.org/Projects/BuildStream/Monthly-Meeting
[BuildStream calendar]: https://calendar.google.com/calendar?cid=Y29kZXRoaW5rLmNvLnVrX21wZ2FoMHVqNTM4aG5ic2Y0bDdiNHJjaHRzQGdyb3VwLmNhbGVuZGFyLmdvb2dsZS5jb20
[reviewing and developing contents]: https://buildstream.gitlab.io/buildstream/main_about.html
[website]: https://gitlab.com/BuildStream/website
