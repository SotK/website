title: Welcome to the BuildStream project
save_as: index.html

BuildStream is a Free Software tool for integrating software stacks.

It takes inspiration, lessons and use-cases from various projects including
Bazel, OBS, Reproducible Builds, Yocto, Baserock, Buildroot, Aboriginal, GNOME
Continuous, JHBuild, Flatpak Builder and Android repo.

BuildStream supports multiple build-systems (e.g. autotools, cmake, cpan, distutils,
make, meson, qmake), and can create outputs in a range of formats (e.g. debian
packages, flatpak runtimes, sysroots, system images) for multiple platforms and
chipsets.

Our core users are application developers and system integrators who create
production-ready software systems that need to be maintained efficiently and
reliably in the long term.

New to BuildStream? Find out more in our
[Frequently Asked Questions](faq.html) page.
